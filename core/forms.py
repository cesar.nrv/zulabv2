from django import forms 
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, PasswordChangeForm, UsernameField, PasswordResetForm, SetPasswordForm
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from .models import GrupoEstudio, GrupoArea, Turno, PacienteCritico, Categoria, Planilla, Tupla, PlanillaFooter, Profesional, Estudio
from django.forms import inlineformset_factory

class RegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'email', )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            }) 

class LoginForm(AuthenticationForm):
    username = UsernameField(widget=forms.TextInput(attrs={"class": "form-control"}))
    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={"autocomplete": "current-password", "class": "form-control"}),
    )

class UserPasswordResetForm(PasswordResetForm):
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'class': 'form-control'
    }))

class UserSetPasswordForm(SetPasswordForm):
    new_password1 = forms.CharField(max_length=50, widget=forms.PasswordInput(attrs={
        'class': 'form-control'
    }), label="New Password")
    new_password2 = forms.CharField(max_length=50, widget=forms.PasswordInput(attrs={
        'class': 'form-control'
    }), label="Confirm New Password")
    

class UserPasswordChangeForm(PasswordChangeForm):
    old_password = forms.CharField(max_length=50, widget=forms.PasswordInput(attrs={
        'class': 'form-control'
    }), label='Old Password')
    new_password1 = forms.CharField(max_length=50, widget=forms.PasswordInput(attrs={
        'class': 'form-control'
    }), label="New Password")
    new_password2 = forms.CharField(max_length=50, widget=forms.PasswordInput(attrs={
        'class': 'form-control'
    }), label="Confirm New Password")


class GrupoAreaForm(forms.ModelForm):
    
    class Meta:
        model   = GrupoArea
        exclude = ['umodificacion','fmodificacion','ucreacion','fcreacion']
        
    def __init__ (self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })
        
        self.fields['estado'].widget.attrs.update({'class': 'form-check-input',})   

    def clean(self): 
        try: 
            rsp = GrupoArea.objects.get( 
                codigo = self.cleaned_data['codigo'].upper()
            ) 
            if not self.instance.pk:
                raise forms.ValidationError('Registro de código de área ya existente')
            elif self.instance.pk != rsp.pk:
                raise forms.ValidationError('Edición no permitida, el código de área coincide con otro registro')
        except GrupoArea.DoesNotExist: 
            pass 
        
        try: 
            rsp = GrupoArea.objects.get( 
                area = self.cleaned_data['area'].upper()
            ) 
            if not self.instance.pk:
                raise forms.ValidationError('Registro de área ya existente')
            elif self.instance.pk != rsp.pk:
                raise forms.ValidationError('Edición no permitida, el área coincide con otro registro')
        except GrupoArea.DoesNotExist: 
            pass  

        return self.cleaned_data

class GrupoEstudioForm(forms.ModelForm):
    
    class Meta:
        model   = GrupoEstudio
        exclude = ['umodificacion','fmodificacion','ucreacion','fcreacion']
        
    def __init__ (self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })
        self.fields['estado'].widget.attrs.update({'class': 'form-check-input',}) 
        
    def clean(self):
        try:
            rsp = GrupoEstudio.objects.get(
                grupoEstudio = self.cleaned_data['grupoEstudio'].upper(),
                codigo = self.cleaned_data['codigo'].upper(), 
            )
            if not self.instance.pk:
                raise forms.ValidationError('Registro de estudio ya existente')
            elif self.instance.pk != rsp.pk:
                raise forms.ValidationError('Edición no permitida, el estudio coincide con otro registro')
        except GrupoEstudio.DoesNotExist:
            pass
        return self.cleaned_data

class TurnoForm(forms.ModelForm):
    
    class Meta:
        model   = Turno
        exclude = ['umodificacion','fmodificacion','ucreacion','fcreacion']
        
    def __init__ (self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })
        self.fields['estado'].widget.attrs.update({'class': 'form-check-input',})   

    def clean(self):
        try:
            rsp = Turno.objects.get(
                turno = self.cleaned_data['turno'].upper(),
                codigo = self.cleaned_data['codigo'].upper(), 
            )
            if not self.instance.pk:
                raise forms.ValidationError('Registro de turno ya existente')
            elif self.instance.pk != rsp.pk:
                raise forms.ValidationError('Edición no permitida, el turno coincide con otro registro')
        except Turno.DoesNotExist:
            pass
        return self.cleaned_data

class PacienteCriticoForm(forms.ModelForm):
    
    class Meta:
        model   = PacienteCritico
        exclude = ['umodificacion','fmodificacion','ucreacion','fcreacion']
        
    def __init__ (self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })
        self.fields['estado'].widget.attrs.update({'class': 'form-check-input',})   
    
    def clean(self):
        try:
            rsp = PacienteCritico.objects.get(
                PacienteCritico = self.cleaned_data['nombre'].upper(), 
            )
            if not self.instance.pk:
                raise forms.ValidationError('Registro de paciente crítico ya existente')
            elif self.instance.pk != rsp.pk:
                raise forms.ValidationError('Edición no permitida, el paciente crítico coincide con otro registro')
        except PacienteCritico.DoesNotExist:
            pass
        return self.cleaned_data

class CategoriaForm(forms.ModelForm):
    
    class Meta:
        model   = Categoria
        exclude = ['umodificacion','fmodificacion','ucreacion','fcreacion']
        
    def __init__ (self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })
        self.fields['estado'].widget.attrs.update({'class': 'form-check-input',})   

    def clean(self):
        try:
            rsp = Categoria.objects.get(
                categoria = self.cleaned_data['categoria'].upper(), 
            )
            if not self.instance.pk:
                raise forms.ValidationError('Registro de categoria ya existente')
            elif self.instance.pk != rsp.pk:
                raise forms.ValidationError('Edición no permitida, la categoria coincide con otro registro')
        except Categoria.DoesNotExist:
            pass
        return self.cleaned_data

class ProfesionalForm(forms.ModelForm):
    
    class Meta:
        model   = Profesional
        exclude = ['umodificacion','fmodificacion','ucreacion','fcreacion']
        
    def __init__ (self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })
        self.fields['estado'].widget.attrs.update({'class': 'form-check-input',})   

    def clean(self):
        try:
            rsp = Profesional.objects.get(
                nombre = self.cleaned_data['nombre'].upper(), 
            )
            if not self.instance.pk:
                raise forms.ValidationError('Registro de profesional ya existente')
            elif self.instance.pk != rsp.pk:
                raise forms.ValidationError('Edición no permitida, el profesional coincide con otro registro')
        except Profesional.DoesNotExist:
            pass
        return self.cleaned_data

class PlanillaForm(forms.ModelForm):
    turno = forms.ModelChoiceField(queryset=Turno.objects.filter(estado=True)) 
    categoria = forms.ModelChoiceField(queryset=Categoria.objects.filter(estado=True).order_by('categoria'))  

    class Meta:
        model   = Planilla
        exclude = ['umodificacion','fmodificacion','ucreacion','fcreacion']
        
    def __init__ (self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            }) 
        self.fields['fecha'].widget.attrs.update({'class': 'form-control datepicker',}) 
        self.fields['estado'].widget.attrs.update({'class': 'form-check-input',}) 
        self.fields['categoria'].empty_label = "Seleccione una categoria."  
        self.fields['categoria'].initial = 1
        self.fields['turno'].empty_label = "Seleccione el turno." 
        """ self.fields['turno'].initial = 1 """
        
class TuplaForm(forms.ModelForm):
    planilla = forms.ModelChoiceField(queryset=Planilla.objects.filter(estado=True))   
    estudio = forms.ModelChoiceField(queryset=Estudio.objects.filter(estado=True).order_by('estudio'))  
    grupoArea = forms.ModelChoiceField(queryset=GrupoArea.objects.filter(estado=True).order_by('area'))  
    
    class Meta:
        model   = Tupla
        exclude = ['umodificacion','fmodificacion','ucreacion','fcreacion']
        
    def __init__ (self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })  
        self.fields['estado'].widget.attrs.update({'class': 'form-check-input',})   

class EstudioForm(forms.ModelForm): 
    grupoEstudio = forms.ModelChoiceField(queryset=GrupoEstudio.objects.filter(estado=True))   
    profesional = forms.ModelChoiceField(required=False, queryset=Profesional.objects.filter(estado=True).order_by('nombre'))   
    
    class Meta:
        model   = Estudio
        exclude = ['umodificacion','fmodificacion','ucreacion','fcreacion']
        
    def __init__ (self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })  
        self.fields['estado'].widget.attrs.update({'class': 'form-check-input',})  
        self.fields['grupoEstudio'].empty_label = "Seleccione un grupo"
        self.fields['profesional'].empty_label = "Si corresponde, seleccione un profesional"
    def clean(self):
        try:
            rsp = Estudio.objects.get(
                estudio = self.cleaned_data['estudio'], 
            )
            if not self.instance.pk:
                raise forms.ValidationError('Registro de estudio ya existente')
            elif self.instance.pk != rsp.pk:
                raise forms.ValidationError('Edición no permitida, el estudio coincide con otro registro')
        except Estudio.DoesNotExist:
            pass
        return self.cleaned_data
    
class PlanillaFooterForm(forms.ModelForm):
    pacientesCriticos = forms.ModelChoiceField(queryset=PacienteCritico.objects.filter(estado=True).order_by('nombre'))   
    planilla = forms.ModelChoiceField(queryset=Planilla.objects.filter(estado=True)) 
    
    class Meta:
        model   = PlanillaFooter
        exclude = ['umodificacion','fmodificacion','ucreacion','fcreacion']
        
    def __init__ (self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })   
        self.fields['estado'].widget.attrs.update({'class': 'form-check-input',})  

TuplaFormSet = inlineformset_factory(
    Planilla, Tupla, form=TuplaForm, extra=0,  can_delete=False, can_delete_extra=False)
PlanillaFooterFormSet = inlineformset_factory(
    Planilla, PlanillaFooter, form=PlanillaFooterForm, extra=0,  can_delete=False, can_delete_extra=False)