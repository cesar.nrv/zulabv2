from django.shortcuts import render, redirect, get_object_or_404 
from django.db import DatabaseError, IntegrityError
from django.db.models import Prefetch
from django.contrib.auth.models import User
from django.core.serializers import serialize
from django.urls import reverse_lazy 
from django.views import generic 
from django.http import HttpResponse 
from django.contrib.auth import logout
from django.utils import timezone
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth import views as auth_views
from django.db.models import Sum
from django.db.models.functions import Concat
from django.db.models import Value
from django.db import models 
from .models import GrupoArea, GrupoEstudio, Turno, Categoria, Profesional, Estudio, Planilla, Tupla, PlanillaFooter
from .forms import GrupoAreaForm, GrupoEstudioForm, TurnoForm, CategoriaForm, ProfesionalForm, EstudioForm, PlanillaForm, PlanillaFooterFormSet, TuplaFormSet,\
                    LoginForm, RegistrationForm, UserPasswordResetForm, UserSetPasswordForm, UserPasswordChangeForm
import json 
from django.http import JsonResponse
from datetime import datetime 
from django.db.models import DateField
from django.db.models.functions import Trunc
from dateutil.relativedelta import relativedelta 

import pytz

class MixinFormInvalid:
  def form_invalid(self,form):
    isAjax = self.request.headers.get('X-Requested-With') == 'XMLHttpRequest'
    if isAjax:
      errors = form.errors.as_json()
      return JsonResponse({'errors':errors},status = 400)
    else:
      return super().form_invalid(form)

class SinPrivilegios(LoginRequiredMixin, PermissionRequiredMixin, MixinFormInvalid):
  permission_required = ['view_planilla'] 
  login_url = 'login'
  raise_exception=False
  redirect_field_name="redirecto_to"
  
class Home(SuccessMessageMixin, SinPrivilegios, generic.TemplateView):
  login_url = "login" 
  template_name="pages/index.html"   

  def handle_no_permission(self):
    from django.contrib.auth.models import AnonymousUser
    if not self.request.user==AnonymousUser():
      self.login_url='login'
    return HttpResponseRedirect(reverse_lazy(self.login_url))
  
@login_required(login_url='/accounts/login/')  
def GetTotalAtencionesMensual(request): 
  ffin = timezone.now() 
  fini = ffin.replace(day=1, hour=0, minute=0, second=0, microsecond=0)  
  tuplas = Tupla.objects.values_list('planilla__turno__codigo').annotate(
  suma_atenciones=Sum('cantidad')).filter(estado = True, planilla__fecha__range=(fini, ffin)).order_by('planilla__turno__codigo')  
  resp = {}    
  try: 
    resp ['tm_arrayData'] = tuplas[0][1]
  except IndexError: 
      resp ['tm_arrayData'] = 0
  try: 
    resp ['tt_arrayData'] = tuplas[1][1]
  except IndexError: 
      resp ['tt_arrayData'] = 0
  try: 
    resp ['tn_arrayData'] = tuplas[2][1]
  except IndexError: 
      resp ['tn_arrayData'] = 0 

  return JsonResponse(resp, safe=False)   

""" ### ENTIDADES ### """
class ProfesionalView (SinPrivilegios, generic.ListView):
    permission_required = 'view_profesional'
    model = Profesional
    template_name = 'pages/profesional_listado.html'
    context_object_name = 'obj'

class ProfesionalNew (SinPrivilegios,   generic.CreateView):
    permission_required = 'add_profesional'
    model = Profesional
    template_name = 'pages/profesional_form.html'
    context_object_name = 'obj'
    form_class = ProfesionalForm
    success_url = reverse_lazy('profesional_listado')
    success_message="Profesional creado correctamente" 
    
    def form_valid(self, form):
      form.instance.ucreacion = self.request.user
      return super().form_valid(form)
    
    def get_context_data(self, **kwargs):
      context = super(ProfesionalNew, self).get_context_data(**kwargs) 
      return context  

class ProfesionalEdit (SuccessMessageMixin, SinPrivilegios, generic.UpdateView):
  permission_required = 'change_profesional'
  model = Profesional
  template_name = 'pages/profesional_form.html'
  context_object_name = 'obj'
  form_class = ProfesionalForm
  success_url = reverse_lazy('profesional_listado') 
  success_message="Profesional modificado correctamente" 

  def form_valid(self, form):
    form.instance.umodificacion = self.request.user.id
    form.instance.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z') 
    return super().form_valid(form) 

@login_required(login_url='/login/')
@permission_required('change_profesional', login_url='login')
def ProfesionalInactivar(request, id):
  resp = Profesional.objects.filter(pk=id).first()
  
  if request.method=='POST':
    if resp:
      resp.estado = not resp.estado
      resp.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z') 
      resp.save()
      return HttpResponse('OK')
    return HttpResponse('FAIL')
  return HttpResponse('FAIL') 

class TurnoView (SinPrivilegios, generic.ListView):
    permission_required = 'view_turno'
    model = Turno
    template_name = 'pages/turno_listado.html'
    context_object_name = 'obj'

class TurnoNew (SinPrivilegios,   generic.CreateView):
    permission_required = 'add_turno'
    model = Turno
    template_name = 'pages/turno_form.html'
    context_object_name = 'obj'
    form_class = TurnoForm
    success_url = reverse_lazy('turno_listado')
    success_message="Área creada correctamente" 
    
    def form_valid(self, form):
      form.instance.ucreacion = self.request.user
      return super().form_valid(form)
    
    def get_context_data(self, **kwargs):
      context = super(TurnoNew, self).get_context_data(**kwargs) 
      return context  

class TurnoEdit (SuccessMessageMixin, SinPrivilegios, generic.UpdateView):
  permission_required = 'change_turno'
  model = Turno
  template_name = 'pages/turno_form.html'
  context_object_name = 'obj'
  form_class = TurnoForm
  success_url = reverse_lazy('turno_listado') 
  success_message="Turno modificado correctamente" 

  def form_valid(self, form):
    form.instance.umodificacion = self.request.user.id
    form.instance.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z') 
    return super().form_valid(form) 

@login_required(login_url='/login/')
@permission_required('change_turno', login_url='login')
def TurnoInactivar(request, id):
  resp = Turno.objects.filter(pk=id).first()
  
  if request.method=='POST':
    if resp:
      resp.estado = not resp.estado
      resp.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z') 
      resp.save()
      return HttpResponse('OK')
    return HttpResponse('FAIL')
  return HttpResponse('FAIL') 

class CategoriaView (SinPrivilegios, generic.ListView):
    permission_required = 'view_categoria'
    model = Categoria
    template_name = 'pages/categoria_listado.html'
    context_object_name = 'obj'

class CategoriaNew (SinPrivilegios,   generic.CreateView):
    permission_required = 'add_categoria'
    model = Categoria
    template_name = 'pages/categoria_form.html'
    context_object_name = 'obj'
    form_class = CategoriaForm
    success_url = reverse_lazy('categoria_listado')
    success_message="Categoía creada correctamente" 
    
    def form_valid(self, form):
      form.instance.ucreacion = self.request.user
      return super().form_valid(form)
    
    def get_context_data(self, **kwargs):
      context = super(CategoriaNew, self).get_context_data(**kwargs) 
      return context  

class CategoriaEdit (SuccessMessageMixin, SinPrivilegios, generic.UpdateView):
  permission_required = 'change_categoria'
  model = Categoria
  template_name = 'pages/categoria_form.html'
  context_object_name = 'obj'
  form_class = CategoriaForm
  success_url = reverse_lazy('categoria_listado') 
  success_message="Categoria modificado correctamente" 

  def form_valid(self, form):
    form.instance.umodificacion = self.request.user.id
    form.instance.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z') 
    return super().form_valid(form) 

@login_required(login_url='/accounts/login/')
@permission_required('change_categoria', login_url='login')
def CategoriaInactivar(request, id):
  resp = Categoria.objects.filter(pk=id).first()
  
  if request.method=='POST':
    if resp:
      resp.estado = not resp.estado
      resp.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z') 
      resp.save()
      return HttpResponse('OK')
    return HttpResponse('FAIL')
  return HttpResponse('FAIL') 

class GrupoAreaView (SinPrivilegios, generic.ListView):
    permission_required = 'view_grupoarea'
    model = GrupoArea
    template_name = 'pages/areas_listado.html'
    context_object_name = 'obj'

class GrupoAreaNew (SinPrivilegios,   generic.CreateView):
    permission_required = 'add_grupoarea'
    model = GrupoArea
    template_name = 'pages/areas_form.html'
    context_object_name = 'obj'
    form_class = GrupoAreaForm
    success_url = reverse_lazy('grupoArea_listado')
    success_message="Área creada correctamente" 
    
    def form_valid(self, form):
      form.instance.ucreacion = self.request.user
      return super().form_valid(form)
    
    def get_context_data(self, **kwargs):
      context = super(GrupoAreaNew, self).get_context_data(**kwargs) 
      return context  

class GrupoAreaEdit (SuccessMessageMixin, SinPrivilegios, generic.UpdateView):
  permission_required = 'change_grupoarea'
  model = GrupoArea
  template_name = 'pages/areas_form.html'
  context_object_name = 'obj'
  form_class = GrupoAreaForm
  success_url = reverse_lazy('grupoArea_listado') 
  success_message="Área modificada correctamente" 

  def form_valid(self, form):
    form.instance.umodificacion = self.request.user.id
    form.instance.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z') 
    return super().form_valid(form) 

@login_required(login_url='/accounts/login/')
@permission_required('change_grupoarea', login_url='login')
def GrupoAreaInactivar(request, id):
  resp = GrupoArea.objects.filter(pk=id).first()
  
  if request.method=='POST':
    if resp:
      resp.estado = not resp.estado
      resp.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z') 
      resp.save()
      return HttpResponse('OK')
    return HttpResponse('FAIL')
  return HttpResponse('FAIL') 

class EstudioView (SinPrivilegios, generic.ListView):
    permission_required = 'view_estudio'
    model = Estudio
    template_name = 'pages/estudio_listado.html'
    context_object_name = 'obj'

class EstudioNew (SinPrivilegios,   generic.CreateView):
    permission_required = 'add_estudio'
    model = Estudio
    template_name = 'pages/estudio_form.html'
    context_object_name = 'obj'
    form_class = EstudioForm
    success_url = reverse_lazy('estudio_listado')
    success_message="Estudio creado correctamente" 
    
    def form_valid(self, form):
      form.instance.ucreacion = self.request.user
      return super().form_valid(form)
    
    def get_context_data(self, **kwargs):
      context = super(EstudioNew, self).get_context_data(**kwargs) 
      return context  

class EstudioEdit (SuccessMessageMixin, SinPrivilegios, generic.UpdateView):
  permission_required = 'change_estudio'
  model = Estudio
  template_name = 'pages/estudio_form.html'
  context_object_name = 'obj'
  form_class = EstudioForm
  success_url = reverse_lazy('estudio_listado') 
  success_message="Estudio modificado correctamente" 

  def form_valid(self, form):
    form.instance.umodificacion = self.request.user.id
    form.instance.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z') 
    return super().form_valid(form) 

@login_required(login_url='/accounts/login/')
@permission_required('change_estudio', login_url='login')
def EstudioInactivar(request, id):
  resp = Estudio.objects.filter(pk=id).first()
  
  if request.method=='POST':
    if resp:
      resp.estado = not resp.estado
      resp.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z') 
      resp.save()
      return HttpResponse('OK')
    return HttpResponse('FAIL')
  return HttpResponse('FAIL') 

class GrupoEstudioView (SinPrivilegios, generic.ListView):
    permission_required = 'view_grupoestudio'
    model = GrupoEstudio
    template_name = 'pages/grupoestudio_listado.html'
    context_object_name = 'obj'

class GrupoEstudioNew (SinPrivilegios,   generic.CreateView):
    permission_required = 'add_grupoestudio'
    model = GrupoEstudio
    template_name = 'pages/grupoestudio_form.html'
    context_object_name = 'obj'
    form_class = GrupoEstudioForm
    success_url = reverse_lazy('grupoEstudio_listado')
    success_message="Grupo de estudio creado correctamente" 
    
    def form_valid(self, form):
      form.instance.ucreacion = self.request.user
      return super().form_valid(form)
    
    def get_context_data(self, **kwargs):
      context = super(GrupoEstudioNew, self).get_context_data(**kwargs) 
      return context  

class GrupoEstudioEdit (SuccessMessageMixin, SinPrivilegios, generic.UpdateView):
  permission_required = 'change_grupoestudio'
  model = GrupoEstudio
  template_name = 'pages/grupoestudio_form.html'
  context_object_name = 'obj'
  form_class = GrupoEstudioForm
  success_url = reverse_lazy('grupoEstudio_listado') 
  success_message="Grupo de estudio modificado correctamente" 

  def form_valid(self, form):
    form.instance.umodificacion = self.request.user.id
    form.instance.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z') 
    return super().form_valid(form) 

@login_required(login_url='/accounts/login/')
@permission_required('change_grupoestudio', login_url='login')
def GrupoEstudioInactivar(request, id):
  resp = GrupoEstudio.objects.filter(pk=id).first()
  
  if request.method=='POST':
    if resp:
      resp.estado = not resp.estado
      resp.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z') 
      resp.save()
      return HttpResponse('OK')
    return HttpResponse('FAIL')
  return HttpResponse('FAIL')

class PlanillaView (SinPrivilegios, generic.ListView):
  permission_required = 'view_planilla'
  model = Planilla 
  template_name = 'pages/planillas_listado.html'
  context_object_name = 'obj'
  def get_queryset(self):
    queryset = super().get_queryset()
    return queryset.order_by('-fecha') 

def EditarPlanilla(request, pk): 
  template_name = 'pages/planilla_form.html'
  planilla = get_object_or_404(Planilla, pk=pk)  
  tupla = Tupla()
  grupoArea = GrupoArea.objects.filter(estado=True).order_by('id')   
  estudio = Estudio.objects.filter(estado=True).order_by('orden')
  return render(request, template_name, {'planilla': planilla, 'tupla': tupla, 'grupoArea': grupoArea, 'estudio': estudio })  
  
class PlanillaNew (SinPrivilegios,   generic.CreateView):
  permission_required = 'add_planilla'
  model = Planilla  
  template_name = 'pages/planilla_form.html'
  context_object_name = 'obj'
  form_class = PlanillaForm
  success_url = reverse_lazy('planillas_listado')
  success_message="Planilla creada correctamente" 
  def get_queryset(self):
    queryset = super().get_queryset()
    return queryset.order_by('id')   

  def get(self, request):
      template_name = 'pages/planilla_form.html'
      planilla = Planilla()
      tupla = Tupla()
      grupoArea = GrupoArea.objects.filter(estado=True).order_by('id')   
      estudio = Estudio.objects.filter(estado=True).order_by('orden')
      return render(request, template_name, {'planilla': planilla, 'grupoArea': grupoArea, 'estudio': estudio, 'tupla':tupla})
  
  def form_valid(self, form):
    form.instance.ucreacion = self.request.user 
    return super().form_valid(form)
  
  def get_context_data(self, **kwargs):
    context = super(PlanillaNew, self).get_context_data(**kwargs) 
    return context  
  
def datetime_to_iso(obj):
    if isinstance(obj, datetime):
        obj_local = obj.astimezone(pytz.timezone('America/La_Paz')) 
        # Obtener el nombre del día y el nombre del mes
        nombre_dia = obj_local.strftime("%A").replace('Monday', 'lunes').replace('Tuesday', 'martes').replace('Wednesday', 'miércoles').replace('Thursday', 'jueves').replace('Friday', 'viernes').replace('Saturday', 'sábado').replace('Sunday', 'domingo')
        nombre_mes = obj_local.strftime("%B").replace('January', 'enero').replace('February', 'febrero').replace('March', 'marzo').replace('April', 'abril').replace('May', 'mayo').replace('June', 'junio').replace('July', 'julio').replace('August', 'agosto').replace('September', 'septiembre').replace('October', 'octubre').replace('November', 'noviembre').replace('December', 'diciembre')
        # Obtener el día del mes y el año
        dia = obj_local.strftime("%d")
        año = obj_local.strftime("%Y")
        hora = obj_local.strftime("%H")
        minuto = obj_local.strftime("%M")
        segundo = obj_local.strftime("%S")
        # Formar la fecha en el formato deseado
        fecha_formateada = f"{nombre_dia}, {dia} de {nombre_mes} de {año}, {hora}:{minuto}:{segundo}"
        return fecha_formateada
    return obj

@login_required(login_url='/accounts/login/')
@permission_required('change_planilla', login_url='login')
def PlanillaInactivar(request, id):
  resp = Planilla.objects.filter(pk=id).first()
  
  if request.method=='POST':
    if resp:
      resp.estado = not resp.estado
      resp.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z') 
      resp.save()
      return HttpResponse('OK')
    return HttpResponse('FAIL')
  return HttpResponse('FAIL')

@login_required(login_url='/accounts/login/') 
def TuplaHistorico(request):
  data_dict = json.loads(request.body.decode('utf-8').replace("'", '"'))    
  tupla_ = Tupla.objects.filter(pk = data_dict['idTupla_'], estado = True)[:1]
  mensaje_error=''
  if tupla_: 
    tuplas_ = Tupla.history.filter(id=tupla_).values_list(
    'history_user_id__username',
    'history_date',
    'planilla',
    'estudio',
    'grupoArea',
    'cantidad',
    'history_type'
    ).order_by('-history_date') 
    try: 
      # Los nombres de los campos basados en los valores que estás extrayendo
      field_names = ['history_user_id__username', 'history_date', 'planilla', 'estudio', 'grupoArea', 'cantidad', 'history_type', 'history_user_id']
      # Convertir cada tupla a un diccionario usando los nombres de los campos como claves
      tuplas_list = [
          {field: datetime_to_iso(value) for field, value in zip(field_names, values)}
          for values in tuplas_
      ] 
      # Serializar a JSON
      serialized_data = json.dumps(tuplas_list)
      return JsonResponse(  serialized_data, safe=False) 
    except IntegrityError as e:
      # Este error se lanza por problemas de integridad, por ejemplo, violaciones de una restricción UNIQUE
      mensaje_error = f"Error de integridad: {e}"
    except DatabaseError as e:
      # Captura otros errores de base de datos
      mensaje_error = f"Error en la base de datos: {e}"
    except Exception as e:
      # Captura cualquier otra excepción
      mensaje_error = f"Error inesperado: {e}" 
    return JsonResponse({'error': str(mensaje_error)})   
  else: 
    return JsonResponse({'mensaje': 'No hay resultados'}) 

@login_required(login_url='/accounts/login/') 
def TuplaVerifica(request):
  data_dict = json.loads(request.body.decode('utf-8').replace("'", '"')) 
  fecha_ = datetime.strptime(data_dict['id_fecha'], '%Y-%m-%d').date()
  turno_ = Turno.objects.get(id = data_dict['id_turno'])
  categoria_ = Categoria.objects.get(id=1)  
  planilla_ = []
  planilla_ = Planilla.objects.filter(fecha = fecha_ , categoria = categoria_, turno = turno_, estado = True)[:1] 
  if planilla_:
    tuplas_ = Tupla.objects.filter(planilla=planilla_)
    try: 
      serialized_queryset = serialize('json', tuplas_)
      return JsonResponse(  serialized_queryset, safe=False) 
    except Exception as e: 
      return JsonResponse({'error': str(e)}, status=500)
  else: 
    return JsonResponse({'mensaje': 'NH'}) 

@login_required(login_url='/accounts/login/') 
def TuplaSave(request):
  if request.method=='POST': 
    try:
      data_dict = json.loads(request.body.decode('utf-8').replace("'", '"'))  
      fecha_ = datetime.strptime(data_dict['id_fecha'], '%Y-%m-%d').date() 
      """ ARMAMOS EL OBJETO PLANILLA Y GUARDAMOS"""
      #verificamos si existe la planilla
      turno_ = Turno.objects.get(id = data_dict['id_turno'])
      categoria_ = Categoria.objects.get(id=1) 

      auxPlanilla = Planilla.objects.filter(fecha = fecha_ , categoria = categoria_, turno = turno_, estado = True)[:1] 
      planilla_ = Planilla() 
      if not auxPlanilla:  
        planilla_.fecha = fecha_
        planilla_.turno = turno_ 
        planilla_.categoria = categoria_ 
        
        planilla_.observaciones = data_dict['observaciones_']  
        planilla_.estadoEquipos = data_dict['estadoEquipos_']  
        planilla_.estadoAreas = data_dict['estadoAreas_']  
        planilla_.pacientesCriticos = data_dict['pacientesCriticos_'] 

        planilla_.ucreacion = request.user 
        planilla_.save()
      else: 
        planilla_ = Planilla.objects.get(fecha = fecha_ , categoria = categoria_, turno = turno_, estado = True)
        sw = False 
        if planilla_.observaciones != data_dict['observaciones_']:
          planilla_.observaciones = data_dict['observaciones_']  
          sw = True
        if planilla_.estadoEquipos != data_dict['estadoEquipos_']:
          planilla_.estadoEquipos = data_dict['estadoEquipos_']  
          sw = True
        if planilla_.estadoAreas != data_dict['estadoAreas_']:
          planilla_.estadoAreas = data_dict['estadoAreas_']  
          sw = True
        if planilla_.pacientesCriticos != data_dict['pacientesCriticos_']:
          planilla_.pacientesCriticos = data_dict['pacientesCriticos_']    
          sw = True
        if sw: 
          planilla_.umodificacion = request.user.id
          planilla_.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z')  
          planilla_.save()

      """ VERIFICAMOS SI EXISTE LA TUPLA PARA EDITARLA O CREARLA"""
      tupla = Tupla()
      if data_dict['id_estudio'] and data_dict['id_area']:
        estudio_ = Estudio.objects.get(id = data_dict['id_estudio']) 
        grupoArea_ = GrupoArea.objects.get(id = data_dict['id_area']) 
        auxTupla = Tupla.objects.filter(estudio = estudio_, grupoArea = grupoArea_, planilla = planilla_ , estado = True)[:1] 
        if not auxTupla:
          tupla.estudio = Estudio.objects.get(id = data_dict['id_estudio']) 
          tupla.grupoArea = GrupoArea.objects.get(id = data_dict['id_area']) 
          tupla.planilla = planilla_
          tupla.cantidad = data_dict['valor']
          tupla.ucreacion = request.user
        else:
          tupla = Tupla.objects.get(estudio = estudio_, grupoArea = grupoArea_, planilla = planilla_ , estado = True)  
          tupla.cantidad = data_dict['valor']
          tupla.umodificacion = request.user.id
          tupla.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z')  
        tupla.save()

      return JsonResponse({'mensaje': 'OK'})
      
    except IntegrityError as e:
      # Este error se lanza por problemas de integridad, por ejemplo, violaciones de una restricción UNIQUE
      mensaje_error = f"Error de integridad: {e}"
    except DatabaseError as e:
      # Captura otros errores de base de datos
      mensaje_error = f"Error en la base de datos: {e}"
    except Exception as e:
      # Captura cualquier otra excepción
      mensaje_error = f"Error inesperado: {e}" 
    return JsonResponse({'error': str(mensaje_error)})   
  else:
    return JsonResponse({'error': 'Método no permitido'}) 

def getDataChartArea(fini, ffin):
  querysetDate = Planilla.objects.annotate(
                  fecha_formateada=Concat(
                    'fecha__day', Value('/'), 'fecha__month', Value('/'), 'fecha__year',
                    output_field=models.CharField()
                  )
                ).filter(estado=True, fecha__range=(fini, ffin)).order_by('fecha').distinct()
  resultados = querysetDate.values_list('fecha_formateada', flat=True) 
  queryset = Planilla.objects.annotate(
              suma_atenciones=Sum('tupla__cantidad')).annotate(
                fecha_formateada=Concat(
                  'fecha__day', Value('/'), 'fecha__month', Value('/'), 'fecha__year',
                  output_field=models.CharField()
              )).filter(estado = True, fecha__range=(fini, ffin)).order_by('fecha')  
  context = {}  
  context ['areaChartTm_arrayLabel'] = [] 
  context ['areaChartTm_arrayData'] = [] 
  context ['areaChartTt_arrayData'] = []
  context ['areaChartTn_arrayData'] = []
  for itemDate in resultados:    
    context ['areaChartTm_arrayLabel'].append(str(itemDate))
    context ['areaChartTm_arrayData'].append(str(getValorFecha(queryset,itemDate, 'TM'))) 
    context ['areaChartTt_arrayData'].append(str(getValorFecha(queryset,itemDate, 'TT'))) 
    context ['areaChartTn_arrayData'].append(str(getValorFecha(queryset,itemDate, 'TN')))
  return context
  
def getValorFecha(datos, fecha, turno):
  resp = 0
  for item in datos: 
      if item.fecha_formateada == fecha and item.turno.codigo == turno:   
          resp = item.suma_atenciones
  return resp

def getDataChartBarHorizontalArea(fini, ffin):
  
  querysetGrupoArea = GrupoArea.objects.filter(estado = True).order_by('id') 
  tuplas = Tupla.objects.values_list('grupoArea__area', 'planilla__turno__codigo').annotate(suma_atenciones=Sum('cantidad')).filter(estado = True, planilla__fecha__range=(fini, ffin))
  resultados = querysetGrupoArea.values_list('area', flat=True)
  resp = {}  
  resp ['barHorzTm_arrayLabel'] = [] 
  resp ['barHorzTm_arrayData'] = [] 
  resp ['barHorzTt_arrayData'] = []
  resp ['barHorzTn_arrayData'] = []
  for itemArea in resultados:    
    resp ['barHorzTm_arrayLabel'].append(str(itemArea))
    resp ['barHorzTm_arrayData'].append(str(getValorEstBarHorz(tuplas,itemArea, 'TM'))) 
    resp ['barHorzTt_arrayData'].append(str(getValorEstBarHorz(tuplas,itemArea, 'TT'))) 
    resp ['barHorzTn_arrayData'].append(str(getValorEstBarHorz(tuplas,itemArea, 'TN'))) 
  return resp   

def getDataChartBarHorizontalEst(fini, ffin):
  
  querysetGrupoEst = GrupoEstudio.objects.filter(estado = True).order_by('estudio__orden') 
  resultados = querysetGrupoEst.values_list('grupoEstudio', flat=True).distinct()
  tuplas = Tupla.objects.values_list('estudio__grupoEstudio__grupoEstudio', 'planilla__turno__codigo').annotate(suma_atenciones=Sum('cantidad')).filter(estado = True, planilla__fecha__range=(fini, ffin))

  resp = {}  
  resp ['barHorzEst_arrayLabel'] = [] 
  resp ['barHorzEstTm_arrayData'] = [] 
  resp ['barHorzEstTt_arrayData'] = []
  resp ['barHorzEstTn_arrayData'] = []
  for itemEst in resultados:    
    resp ['barHorzEst_arrayLabel'].append(str(itemEst))
    resp ['barHorzEstTm_arrayData'].append(str(getValorEstBarHorz(tuplas,itemEst, 'TM'))) 
    resp ['barHorzEstTt_arrayData'].append(str(getValorEstBarHorz(tuplas,itemEst, 'TT'))) 
    resp ['barHorzEstTn_arrayData'].append(str(getValorEstBarHorz(tuplas,itemEst, 'TN'))) 

  return resp 

def getValorEstBarHorz(tuplas, filtro, turno):
  resp = 0    
  for tupla in tuplas:  
    if tupla[0] == filtro and tupla[1] == turno:   
        resp = tupla[2]
  return resp 

def getDataChartBarVerticalEst(fini, ffin):
  
  querysetGrupoEst = Estudio.objects.filter(estado = True).order_by('orden') 
  resultados = querysetGrupoEst.values_list('id', 'estudio').distinct()
  tuplas = Tupla.objects.values_list('estudio__estudio', 'planilla__turno__codigo').annotate(suma_atenciones=Sum('cantidad')).filter(estado = True, planilla__fecha__range=(fini, ffin))
  
  resp = {}  
  resp ['verticalEstudio'] = [] 
  for itemEst in resultados:    
    resp ['verticalEstudio'].append([ 
                              str(itemEst[0]) ,
                              str(itemEst[1]), 
                              str(getValorEstBarVert(tuplas,itemEst[1], 'TM')), 
                              str(getValorEstBarVert(tuplas,itemEst[1], 'TT')),
                              str(getValorEstBarVert(tuplas,itemEst[1], 'TN'))])   
  return resp 

def getValorEstBarVert(tuplas, filtro, turno):
  valor = 0    
  for tupla in tuplas:  
    if tupla[0] == filtro and tupla[1] == turno:   
      valor = tupla[2]
  return valor 

def getDataPolarEst(fini, ffin):  
  tuplas = Tupla.objects.values('estudio__grupoEstudio__grupoEstudio').annotate(suma_atenciones=Sum('cantidad')).filter(estado = True, planilla__fecha__range=(fini, ffin))
  resp = {}  
  resp ['polarEst_arrayLabel'] = [] 
  resp ['polarEst_arrayData'] = []
  for tupla in tuplas:    
    resp ['polarEst_arrayLabel'].append(str(tupla['estudio__grupoEstudio__grupoEstudio']))
    resp ['polarEst_arrayData'].append(str( tupla['suma_atenciones']))   
  return resp  

def getDataPolarArea(fini, ffin):  
  tuplas = Tupla.objects.values('grupoArea__area').annotate(suma_atenciones=Sum('cantidad')).filter(estado = True, planilla__fecha__range=(fini, ffin))
  resp = {}  
  resp ['polarArea_arrayLabel'] = [] 
  resp ['polarArea_arrayData'] = []
  for tupla in tuplas:    
    resp ['polarArea_arrayLabel'].append(str(tupla['grupoArea__area']))
    resp ['polarArea_arrayData'].append(str( tupla['suma_atenciones']))   
  return resp  

@login_required(login_url='/accounts/login/') 
def DashboardAsinc(request): 
  data_dict = json.loads(request.body.decode('utf-8').replace("'", '"')) 

  fini =  datetime.strptime(data_dict['fechaini'], '%Y-%m-%d') 
  ffin = datetime.strptime(data_dict['fechafin'], '%Y-%m-%d')    
  serialized_queryset = getDataChartArea(fini, ffin)   
  serialized_queryset.update(getDataChartBarHorizontalArea(fini, ffin))
  serialized_queryset.update(getDataChartBarHorizontalEst(fini, ffin))   
  serialized_queryset.update(getDataPolarEst(fini, ffin))  
  serialized_queryset.update(getDataPolarArea(fini, ffin))   
  serialized_queryset.update(getDataChartBarVerticalEst(fini, ffin))
  return JsonResponse(serialized_queryset, safe=False)  

class Dashboard(SuccessMessageMixin, SinPrivilegios, generic.TemplateView):
    permission_required = ['view_planilla']     
    #login_url = "login"
    template_name='pages/chart_solicitudes.html'
    context_object_name = 'obj'  
    def get_context_data(self, **kwargs):  
        context = super().get_context_data(**kwargs) 
        ffin = datetime.now() 
        fini = ffin - relativedelta(months=1)    
        context.update(getDataChartArea(fini, ffin)) 
        context.update(getDataChartBarHorizontalArea(fini, ffin)) 
        context.update(getDataChartBarHorizontalEst(fini, ffin)) 
        context.update(getDataPolarEst(fini, ffin))
        context.update(getDataPolarArea(fini, ffin)) 
        context.update(getDataChartBarVerticalEst(fini, ffin))
        return context   

""" CODIGO PARA USAR FORMSET A MEDIO REALIZAR """
class PlanillaInline(SuccessMessageMixin, SinPrivilegios):
    permission_required = ['change_planilla', 'change_tupla', 'change_planillafooter']
    form_class = PlanillaForm
    model = Planilla
    context_object_name = 'obj'
    template_name = "matriz.html"

    def form_valid(self, form):  
        if not form.instance.id:
            form.instance.ucreacion = self.request.user
            form.instance.fcreacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z') 
        else:
            form.instance.umodificacion = self.request.user.id
            form.instance.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z') 
            form.instance.ucreacion = self.request.user 
        
        named_formsets = self.get_named_formsets()   

        for x in named_formsets.values():   
            auxResp = x.is_valid()    
            if not auxResp:  
                form_errors = x.errors 
                for itemError in form_errors:  
                    form.errors['__all__'] = itemError 
                context_data = self.get_context_data(form=form)
                return self.render_to_response(context_data)    
            
        self.object = form.save() 

        for name, formset in named_formsets.items():
            aux = 'formset_{0}_valid'.format(name) 
            formset_save_func = getattr(self,aux , None)
            if formset_save_func is not None:
                formset_save_func(formset)
            else:
                formset.save() 
        return redirect('planillas')

    def formset_tuplas_valid(self, formset):  
        tuplas = formset.save(commit=False)   
        for tuppla in tuplas:
            if not tuppla.id:
                tuppla.ucreacion = self.request.user
                tuppla.fcreacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z')  
            else:
                tuppla.umodificacion = self.request.user.id
                tuppla.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z')  
            tuppla.planilla = self.object
            tuppla.save()

    def formset_planillaFooters_valid(self, formset):  
        planillaFooters = formset.save(commit=False)    
        for planillaFooter in planillaFooters:
            if not planillaFooter.id:
                planillaFooter.ucreacion = self.request.user
                planillaFooter.fcreacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z')  
            else:
                planillaFooter.umodificacion = self.request.user.id
                planillaFooter.fmodificacion = (timezone.now()).strftime('%Y-%m-%dT%H:%M:%S.%f%z')   
            planillaFooter.planilla = self.object
            planillaFooter.save()

class PlanillaCreate(PlanillaInline, generic.CreateView):  
    
    def get_context_data(self, **kwargs): 
        ctx = super(PlanillaCreate, self).get_context_data(**kwargs)
        ctx['named_formsets'] = self.get_named_formsets()
        ctx['ci'] = self.kwargs.get('ci') 
        return ctx

    def get_named_formsets(self):  
        if self.request.method == "GET":  
                return {
                    'tuplas': TuplaFormSet(prefix='tuplas'),
                    'planillaFooters': PlanillaFooterFormSet(prefix='planillaFooters'),
                }
        else: 
            return {
                'tuplas': TuplaFormSet(self.request.POST or None, self.request.FILES or None, prefix='tuplas'),
                'planillaFooters': PlanillaFooterFormSet(self.request.POST or None, self.request.FILES or None, prefix='planillaFooters'),
            }

class PlanillaUpdate(PlanillaInline, generic.UpdateView):  
    def get_context_data(self, **kwargs): 
        ctx = super(PlanillaUpdate, self).get_context_data(**kwargs)
        ctx['named_formsets'] = self.get_named_formsets()
        return ctx

    def get_named_formsets(self):  
        if self.request.method == 'POST':
            return {
                'tuplas': TuplaFormSet(self.request.POST or None, self.request.FILES or None, instance=self.object, prefix='tuplas'),
                'planillaFooters': PlanillaFooterFormSet(self.request.POST or None, self.request.FILES or None, instance=self.object, prefix='planillaFooters'),
            }
        elif self.request.method == 'GET':
            tuplasAux = TuplaFormSet(self.request.GET or None, self.request.FILES or None, instance=self.object, prefix='tuplas')
            return {
                'tuplas': tuplasAux,
                'planillaFooters': PlanillaFooterFormSet(self.request.GET or None, self.request.FILES or None, instance=self.object, prefix='planillaFooters'),
            }


@login_required(login_url='/accounts/login/') 
def Matriz(request): 
    template_name='pages/planilla.html' 
    mnsj = ''
    if request.method == 'POST': 
        contexto={"mnsj":mnsj}
        return HttpResponse('EN POST:'+mnsj)
    else:   
        mnsj = 'En GET'    
        form = PlanillaForm()
        contexto = {"planilla":form, "mnsj":mnsj} 
    return render(request,template_name,contexto)  

# Authentication
def registration(request):
  if request.method == 'POST':
    form = RegistrationForm(request.POST)
    if form.is_valid():
      form.save() 
      return redirect('/accounts/login/')
    else:
      print("Registration failed!")
  else:
    form = RegistrationForm()
  
  context = {'form': form}
  return render(request, 'accounts/sign-up.html', context)

class UserLoginView(auth_views.LoginView):
  template_name = 'accounts/sign-in.html'
  form_class = LoginForm
  success_url = '/'

class UserPasswordResetView(auth_views.PasswordResetView):
  template_name = 'accounts/password_reset.html'
  form_class = UserPasswordResetForm

class UserPasswordResetConfirmView(auth_views.PasswordResetConfirmView):
  template_name = 'accounts/password_reset_confirm.html'
  form_class = UserSetPasswordForm

class UserPasswordChangeView(auth_views.PasswordChangeView):
  template_name = 'accounts/password_change.html'
  form_class = UserPasswordChangeForm

def user_logout_view(request):
  logout(request)
  return redirect('/accounts/login/')

# Pages
@login_required(login_url='/accounts/login/')
def index(request):
  return render(request, 'pages/index.html')

@login_required(login_url='/accounts/login/')
def contact_us(request):
  return render(request, 'pages/contact-us.html')


def about_us(request):
  return render(request, 'pages/about-us.html')

def author(request):
  return render(request, 'pages/author.html')

def blank(request):
  return render(request, 'pages/blank.html')

def areasForm(request):
  return render(request, 'pages/areas_form.html')

def areasListado(request):
  return render(request, 'pages/areas_listado.html')


# Sections
def presentation(request):
  return render(request, 'sections/presentation.html')
  
def page_header(request):
  return render(request, 'sections/page-sections/hero-sections.html')

def features(request):
  return render(request, 'sections/page-sections/features.html')

def navbars(request):
  return render(request, 'sections/navigation/navbars.html')

def nav_tabs(request):
  return render(request, 'sections/navigation/nav-tabs.html')

def pagination(request):
  return render(request, 'sections/navigation/pagination.html')

def forms(request):
  return render(request, 'sections/input-areas/forms.html')

def inputs(request):
  return render(request, 'sections/input-areas/inputs.html')

def avatars(request):
  return render(request, 'sections/elements/avatars.html')

def badges(request):
  return render(request, 'sections/elements/badges.html')

def breadcrumbs(request):
  return render(request, 'sections/elements/breadcrumbs.html')

def buttons(request):
  return render(request, 'sections/elements/buttons.html')

def dropdowns(request):
  return render(request, 'sections/elements/dropdowns.html')

def progress_bars(request):
  return render(request, 'sections/elements/progress-bars.html')

def toggles(request):
  return render(request, 'sections/elements/toggles.html')

def typography(request):
  return render(request, 'sections/elements/typography.html')

def alerts(request):
  return render(request, 'sections/attention-catchers/alerts.html')

def modals(request):
  return render(request, 'sections/attention-catchers/modals.html')

def tooltips(request):
  return render(request, 'sections/attention-catchers/tooltips-popovers.html')