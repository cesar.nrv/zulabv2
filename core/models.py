from django.db import models
from django.contrib.auth.models import User 
from django.utils import timezone
from simple_history.models import HistoricalRecords
from django.core.validators import MaxValueValidator

class Seguimiento(models.Model):
    estado = models.BooleanField(default=True, verbose_name='Estado de registro',)
    fcreacion = models.DateTimeField(default=timezone.now, null=True, blank=True)
    fmodificacion = models.DateTimeField(default=timezone.now, null=True, blank=True)
    ucreacion = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True, blank=True)
    umodificacion = models.IntegerField(blank=True, null=True) 
    class Meta:
        abstract = True

# IMPLEMENTACIONES
class GrupoArea(Seguimiento):
    area = models.CharField(max_length=150, verbose_name='Descripción del área', unique=True)
    codigo = models.CharField(max_length=100, verbose_name='Abreviatura', unique=True)
    def __str__(self):
        return '{}'.format(self.codigo)
    def save(self):
        self.area = self.area.upper()
        self.codigo = self.codigo.upper()
        super(GrupoArea, self).save()
    class Meta:
        verbose_name_plural= "Áreas" 
        
class GrupoEstudio(Seguimiento):
    grupoEstudio = models.CharField(max_length=150, verbose_name='Descripción de grupo de estudio', unique=True)
    codigo = models.CharField(max_length=100, verbose_name='Abreviatura', unique=True)
    color = models.CharField(max_length=100, verbose_name='Color', null=True, blank=True)
    def __str__(self):
        return '{}'.format(self.grupoEstudio)
    def save(self):
        self.grupoEstudio = self.grupoEstudio.upper()
        self.codigo = self.codigo.upper()
        super(GrupoEstudio, self).save()
    class Meta:
        verbose_name_plural= "Grupos de estudio"

class Profesional(Seguimiento):
    nombre = models.CharField(max_length=450, verbose_name='Nombre completo', unique=True)
    cargo = models.CharField(max_length=450, verbose_name='Cargo', unique=True)
    contacto = models.IntegerField(validators=[MaxValueValidator(79999999)], verbose_name="Numero de celular", null=True, blank=True)
    def __str__(self):
        return '{}'.format(self.nombre)
    def save(self):
        self.nombre = self.nombre.upper()
        self.cargo = self.cargo.upper()
        super(Profesional, self).save()
    class Meta:
        verbose_name_plural= "Grupos de estudio"

class Estudio(Seguimiento):
    estudio = models.CharField(max_length=150, verbose_name='Descripción de estudio', unique=True) 
    grupoEstudio = models.ForeignKey(GrupoEstudio, on_delete=models.DO_NOTHING,   verbose_name="Grupo")
    profesional = models.ForeignKey(Profesional, on_delete=models.DO_NOTHING,   verbose_name="Profesional a cargo", null=True, blank=True)
    orden = models.IntegerField(verbose_name="Orden dentro de la planilla", null=True, blank=True)

    def __str__(self):
        return '{}'.format(self.estudio)
    
    class Meta:
        verbose_name_plural= "Estudio"

class Turno(Seguimiento):
    turno = models.CharField(max_length=150, verbose_name='Descripción de turno', unique=True)
    codigo = models.CharField(max_length=100, verbose_name='Código', unique=True)
    def __str__(self):
        return '{}'.format(self.turno)
    def save(self):
        self.turno = self.turno.upper()
        self.codigo = self.codigo.upper()
        super(Turno, self).save()
    class Meta:
        verbose_name_plural= "Turnos de atención"

class PacienteCritico(Seguimiento):
    nombre = models.CharField(max_length=150, verbose_name='Nombre completo de paciente crítico', unique=True) 
    def __str__(self):
        return '{}'.format(self.nombre)
    def save(self):
        self.nombre = self.nombre.upper() 
        super(PacienteCritico, self).save()
    class Meta:
        verbose_name_plural= "Pacientes críticos"

""" 1RA CATEGORIA -> PACIENTES SUS """
class Categoria(Seguimiento):
    categoria = models.CharField(max_length=150, verbose_name='Descripción de categoría', unique=True)
    codigo = models.CharField(max_length=100, verbose_name='Código', unique=True)
    def __str__(self):
        return '{}'.format(self.codigo)
    def save(self):
        self.categoria = self.categoria.upper()
        self.codigo = self.codigo.upper()
        super(Categoria, self).save()
    class Meta:
        verbose_name_plural= "Categorías"
    
class Planilla(Seguimiento):
    fecha = models.DateField(verbose_name="Fecha de planilla",   )  
    categoria = models.ForeignKey(Categoria, on_delete=models.DO_NOTHING,   verbose_name="Categoria")
    turno = models.ForeignKey(Turno, on_delete=models.DO_NOTHING,   verbose_name="Turno", null=True, blank=True )
    observaciones = models.TextField( verbose_name='Descripción de categoría', null=True, blank=True)
    estadoEquipos = models.TextField( verbose_name='Estado de equipos', null=True, blank=True)
    estadoAreas = models.TextField( verbose_name='Estado de las areas', null=True, blank=True) 
    pacientesCriticos = models.TextField( verbose_name='Pacientes críticos', null=True, blank=True) 
    history = HistoricalRecords() 
    class Meta:
        verbose_name_plural= "Planillas"

class Tupla(Seguimiento):
    planilla = models.ForeignKey(Planilla, on_delete=models.DO_NOTHING,   verbose_name="planilla") 
    estudio = models.ForeignKey(Estudio, on_delete=models.DO_NOTHING,   verbose_name="Estudio")
    grupoArea = models.ForeignKey(GrupoArea, on_delete=models.DO_NOTHING,   verbose_name="Área")
    cantidad = models.IntegerField(verbose_name='Cantidad atentida')
    history = HistoricalRecords()
    class Meta:
        verbose_name_plural= "Tuplas"

class PlanillaFooter(Seguimiento):
    observaciones = models.TextField( verbose_name='Descripción de categoría', null=True, blank=True)
    estadoEquipos = models.TextField( verbose_name='Estado de equipos', null=True, blank=True)
    estadoAreas = models.TextField( verbose_name='Estado de las areas', null=True, blank=True) 
    pacientesCriticos = models.ForeignKey(PacienteCritico, on_delete=models.DO_NOTHING,   verbose_name="Paciente crítico", null=True, blank=True) 
    planilla = models.ForeignKey(Planilla, on_delete=models.DO_NOTHING,   verbose_name="planilla")  
    class Meta:
        verbose_name_plural= "Footer de Planillas" 