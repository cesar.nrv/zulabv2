"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from core.views import CategoriaEdit, CategoriaInactivar, CategoriaNew, CategoriaView, Dashboard, DashboardAsinc, EditarPlanilla, EstudioEdit, EstudioInactivar, EstudioNew, EstudioView, GetTotalAtencionesMensual, GrupoAreaEdit, GrupoAreaInactivar, GrupoAreaNew, GrupoAreaView, GrupoEstudioEdit, GrupoEstudioInactivar, GrupoEstudioNew, GrupoEstudioView, Home, Matriz, PlanillaInactivar, PlanillaNew, PlanillaView, ProfesionalEdit, ProfesionalInactivar, ProfesionalNew, ProfesionalView, TuplaHistorico, TuplaSave, TuplaVerifica, TurnoEdit, TurnoInactivar, TurnoNew, TurnoView, UserLoginView, UserPasswordChangeView, UserPasswordResetConfirmView, UserPasswordResetView, about_us, areasForm, areasListado, author, blank, contact_us, registration, user_logout_view 
from .views import auth_views
from django.contrib.auth import views as auth_views 

urlpatterns = [ 
    path("admin/", admin.site.urls), 
    path('', Home.as_view(), name="index"),
    path('cantTurnos/', GetTotalAtencionesMensual, name="index_cantTurnos"),

    path('planilla/', Matriz, name="planilla"),
    path('planillas/', PlanillaView.as_view(), name="planillas"),
    path('planillas/new', PlanillaNew.as_view(), name='planillas_new'),
    path('planillas/edit/<int:pk>/', EditarPlanilla, name='planillas_edit'), 
    path('planillas/inactivar/<int:id>', PlanillaInactivar, name='planillas_inactivar'),

    path('tupla/save', TuplaSave, name="tupla_save"),
    path('tupla/verifica', TuplaVerifica, name="tupla_verifica"),
    path('tupla/historico', TuplaHistorico, name="tupla_historico"),
    
    path('chart/solicitudes', Dashboard.as_view(), name="chart_solicitudes"),
    path('chart/asincrono', DashboardAsinc, name="chart_asincrono"),

    path('contact-us/', contact_us, name='contact-us'),
    path('about-us/', about_us, name='about-us'),
    path('author/', author, name='author'), 
    path('blank/', blank, name='blank'), 
    path('areasForm/', areasForm, name='areas_form'), 
    path('areasListado/', areasListado, name='areas_listado'),  

    path('grupoArea', GrupoAreaView.as_view(), name='grupoArea_listado'),
    path('grupoArea/new', GrupoAreaNew.as_view(), name='grupoArea_new'),
    path('grupoArea/edit/<int:pk>/', GrupoAreaEdit.as_view(), name='grupoArea_edit'), 
    path('grupoArea/inactivar/<int:id>', GrupoAreaInactivar, name='grupoArea_inactivar'),

    path('grupoEstudio', GrupoEstudioView.as_view(), name='grupoEstudio_listado'),
    path('grupoEstudio/new', GrupoEstudioNew.as_view(), name='grupoEstudio_new'),
    path('grupoEstudio/edit/<int:pk>/', GrupoEstudioEdit.as_view(), name='grupoEstudio_edit'), 
    path('grupoEstudio/inactivar/<int:id>', GrupoEstudioInactivar, name='grupoEstudio_inactivar'),

    path('turno', TurnoView.as_view(), name='turno_listado'),
    path('turno/new', TurnoNew.as_view(), name='turno_new'),
    path('turno/edit/<int:pk>/', TurnoEdit.as_view(), name='turno_edit'), 
    path('turno/inactivar/<int:id>', TurnoInactivar, name='turno_inactivar'),

    path('categoria', CategoriaView.as_view(), name='categoria_listado'),
    path('categoria/new', CategoriaNew.as_view(), name='categoria_new'),
    path('categoria/edit/<int:pk>/', CategoriaEdit.as_view(), name='categoria_edit'), 
    path('categoria/inactivar/<int:id>', CategoriaInactivar, name='categoria_inactivar'),

    path('profesional', ProfesionalView.as_view(), name='profesional_listado'),
    path('profesional/new', ProfesionalNew.as_view(), name='profesional_new'),
    path('profesional/edit/<int:pk>/', ProfesionalEdit.as_view(), name='profesional_edit'), 
    path('profesional/inactivar/<int:id>', ProfesionalInactivar, name='profesional_inactivar'),

    path('estudio', EstudioView.as_view(), name='estudio_listado'),
    path('estudio/new', EstudioNew.as_view(), name='estudio_new'),
    path('estudio/edit/<int:pk>/', EstudioEdit.as_view(), name='estudio_edit'), 
    path('estudio/inactivar/<int:id>', EstudioInactivar, name='estudio_inactivar'),
    


    # Authentication
    path('accounts/login/', UserLoginView.as_view(), name='login'),
    path('accounts/logout/', user_logout_view, name='logout'),
    path('accounts/register/', registration, name='register'),
    path('accounts/password-change/', UserPasswordChangeView.as_view(), name='password_change'),
    path('accounts/password-change-done/', auth_views.PasswordChangeDoneView.as_view(
        template_name='accounts/password_change_done.html'
    ), name="password_change_done" ),
    path('accounts/password-reset/', UserPasswordResetView.as_view(), name='password_reset'),
    path('accounts/password-reset-confirm/<uidb64>/<token>/', 
        UserPasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('accounts/password-reset-done/', auth_views.PasswordResetDoneView.as_view(
        template_name='accounts/password_reset_done.html'
    ), name='password_reset_done'),
    path('accounts/password-reset-complete/', auth_views.PasswordResetCompleteView.as_view(
        template_name='accounts/password_reset_complete.html'
    ), name='password_reset_complete'),
]
