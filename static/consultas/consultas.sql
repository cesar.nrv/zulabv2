select b."grupoEstudio", count(b."grupoEstudio")
from theme_material_kit_estudio as a 
left join theme_material_kit_grupoestudio as b on b.id = a."grupoEstudio_id"
group by b."grupoEstudio"

-- PARA REALIZAR EL RESPALDO DE LA BASE DE DATOS:
pg_dump -U postgres -W -h localhost -d zulab > dbsirmeslp_20240320-previo-actualizar-seg-anillado.sql
-- PARA RESTAURAR
psql -U postgres -h localhost -d zulabbk -f zulab20240410.sql